package com.realmatters.StarWars.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MovieDetails {

    @JsonProperty("Actors")
    private String actors;
    @JsonProperty("Title")
    private String title;

}
