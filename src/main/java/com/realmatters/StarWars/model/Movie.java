package com.realmatters.StarWars.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Movie {

    private String imdbId;
    private MoviePosition position;
    private MovieDetails movieDetails;
    private String cover;

}
