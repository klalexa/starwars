package com.realmatters.StarWars.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequest {

    private String ip;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String params;
    private String api;

}
