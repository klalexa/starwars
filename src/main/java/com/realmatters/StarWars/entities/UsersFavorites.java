package com.realmatters.StarWars.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "users_favorites")
@Getter
@Setter
public class UsersFavorites {

    @Id
    @Column(name = "user_id")
    private String userid;

    @Column(name = "favorite_movie")
    private String favoriteMovie;

    @UpdateTimestamp
    @Column(name = "updated_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTimeStamp = new Date();

    @CreationTimestamp
    @Column(name = "created_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTimeStamp = new Date();

}
