package com.realmatters.StarWars.entities;

import com.realmatters.StarWars.model.UserRequest;
import com.realmatters.StarWars.repository.reqeusthistory.converters.UserRequestConverter;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Request_History")
@Getter
@Setter
public class RequestHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "user_request", length=128)
    @Convert(converter = UserRequestConverter.class)
    private UserRequest userRequest;

    @CreationTimestamp
    @Column(name = "created_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTimeStamp = new Date();

}
