package com.realmatters.StarWars.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "omdb")
@Component
@Getter
@Setter
public class OmdbConfigurations {

    private String apiKey;
    private String apiMovies;
    private String apiDetails;
    private String apiCover;

}
