package com.realmatters.StarWars.config;

import com.realmatters.StarWars.controllers.MovieController;
import com.realmatters.StarWars.services.requestshistory.IRequestHistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class ServiceInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(ServiceInterceptor.class);


    @Autowired
    private IRequestHistoryService historyService;

    @Override
    public boolean preHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        try {
            logger.info("Service Interceptor. Saving request....");
            historyService.save(request);
        } catch(Exception ex) {
            logger.error("Error saving user request ...", ex);
        }
        return super.preHandle(request, response, handler);
    }
}
