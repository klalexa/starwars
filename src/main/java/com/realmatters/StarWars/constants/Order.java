package com.realmatters.StarWars.constants;

public enum Order {

    MACHETE,
    CHRONOLOGICALLY;

    public boolean isMacheteOrder() {
        return this == Order.MACHETE;
    }

}
