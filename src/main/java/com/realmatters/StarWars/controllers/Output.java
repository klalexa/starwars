package com.realmatters.StarWars.controllers;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Output {

    private String response;


    public Output(String response) {
        this.response = response;
    }
}
