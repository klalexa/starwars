package com.realmatters.StarWars.controllers;

import com.realmatters.StarWars.constants.Order;
import com.realmatters.StarWars.model.Movie;
import com.realmatters.StarWars.services.starwars.IStarWarsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/movies")
public class MovieController {

    private static final Logger logger = LoggerFactory.getLogger(MovieController.class);

    @Autowired
    private IStarWarsService starWarsService;

    @GetMapping
    public ResponseEntity<List<Movie>> getAllStarWarsMovies(
                                                        @RequestParam(value = "order", required = false) Order order) {
        try {
            logger.info("Requested for retrieving info for all star wars movies...");
            Order requestedOrder = order == null ? Order.CHRONOLOGICALLY : order;
            List<Movie> movies = starWarsService.getMovies(requestedOrder);
            if (movies == null) {
                logger.info("Internal error");
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            } else if (movies.isEmpty()) {
                logger.info("No movies found");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            } else {
                logger.info("Send response back. ");
                return ResponseEntity.ok(movies);
            }
        }catch (Exception ex) {
            logger.error("Bad request !!!", ex);
            return ResponseEntity.badRequest().build();
        }
    }
}
