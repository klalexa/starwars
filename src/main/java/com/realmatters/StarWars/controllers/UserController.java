package com.realmatters.StarWars.controllers;

import com.realmatters.StarWars.controllers.input.UserFavoriteMovie;
import com.realmatters.StarWars.exceptions.DbException;
import com.realmatters.StarWars.services.requestshistory.IRequestHistoryService;
import com.realmatters.StarWars.services.user.IUsersPreferencesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/users")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(MovieController.class);

    @Autowired
    private IUsersPreferencesService usersPreferencesService;

    @Autowired
    private IRequestHistoryService historyService;

    @PostMapping(value = "/favoriteMovie")
    public ResponseEntity<Output> saveUsersFavoriteMovie(@RequestBody UserFavoriteMovie userFavoriteMovie) {

        try {
            logger.info(String.format("User [%s] Requested to save his favorite [%s ]movie",
                    userFavoriteMovie.getUserid(),
                    userFavoriteMovie.getMovieid()));

            usersPreferencesService.save(userFavoriteMovie);
            String response = String.format("Favorite movie [%s] for user [%s] saved successfully",
                    userFavoriteMovie.getMovieid(), userFavoriteMovie.getUserid());
            logger.info(response);
            return ResponseEntity.ok(new Output(response));
        } catch (DbException dbEx) {
            logger.error("Bad request !!!", dbEx);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception ex) {
            return ResponseEntity.badRequest().build();
        }

    }
}
