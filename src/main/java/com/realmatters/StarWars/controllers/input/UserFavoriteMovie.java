package com.realmatters.StarWars.controllers.input;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserFavoriteMovie {

    @JsonProperty(required = true)
    private String userid;
    @JsonProperty(required = true)
    private String movieid;


}
