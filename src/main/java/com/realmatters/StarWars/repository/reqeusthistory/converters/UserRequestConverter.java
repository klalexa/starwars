package com.realmatters.StarWars.repository.reqeusthistory.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.realmatters.StarWars.model.UserRequest;
import com.realmatters.StarWars.services.omdb.OmdbService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;

public class UserRequestConverter implements AttributeConverter<UserRequest, String> {

    private ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger logger = LoggerFactory.getLogger(OmdbService.class);


    @Override
    public String convertToDatabaseColumn(UserRequest userRequest) {
        try {
            return objectMapper.writeValueAsString(userRequest);
        } catch (JsonProcessingException e) {
            logger.error("Error serialize user request into string for ", e);
            return null;
        }
    }

    @Override
    public UserRequest convertToEntityAttribute(String s) {
        if (StringUtils.isEmpty(s)) {
            return null;
        }

        try {
            return objectMapper.readValue(s, UserRequest.class);
        } catch (Exception e) {
            logger.error("Error deserialize db value into user request ", e);
            return null;
        }
    }
}
