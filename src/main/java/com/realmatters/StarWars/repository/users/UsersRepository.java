package com.realmatters.StarWars.repository.users;

import com.realmatters.StarWars.entities.UsersFavorites;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<UsersFavorites, Long> {
}
