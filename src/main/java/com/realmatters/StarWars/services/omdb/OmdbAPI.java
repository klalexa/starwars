package com.realmatters.StarWars.services.omdb;

import com.realmatters.StarWars.exceptions.OmdbAccessException;
import com.realmatters.StarWars.model.Movie;
import com.realmatters.StarWars.model.MovieDetails;

import java.util.List;
import java.util.Optional;

public interface OmdbAPI {

    Optional<List<Movie>> getStarWarsMovies() throws OmdbAccessException;
    MovieDetails getDetails(String id) throws OmdbAccessException;
    String getCover(String id) throws OmdbAccessException;
}
