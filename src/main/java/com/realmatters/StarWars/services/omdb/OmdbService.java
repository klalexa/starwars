package com.realmatters.StarWars.services.omdb;

import com.realmatters.StarWars.config.OmdbConfigurations;
import com.realmatters.StarWars.constants.ServiceConstants;
import com.realmatters.StarWars.exceptions.OmdbAccessException;
import com.realmatters.StarWars.model.Movie;
import com.realmatters.StarWars.model.MovieDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Optional;

@Service
public class OmdbService implements OmdbAPI {

    private static final Logger logger = LoggerFactory.getLogger(OmdbService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private OmdbConfigurations omdbConfigurations;


    @Override
    public Optional<List<Movie>> getStarWarsMovies() throws OmdbAccessException {
        logger.info("Receiving info for all star wars movies.");
        try {
            ResponseEntity<List<Movie>> response =
                    restTemplate.exchange(omdbConfigurations.getApiMovies(),
                            HttpMethod.GET,
                            HttpEntity.EMPTY,
                            new ParameterizedTypeReference<List<Movie>>() {
                            });

            logger.info(String.format("Info for all movies retrieved with status [%s]", response.getStatusCode()));

            return Optional.ofNullable(response.getBody());

        } catch (Exception ex) {
            logger.error("Error occur during a call to OMDB API ." , ex);
            throw new OmdbAccessException("Error on call of list of movies", ex);
        }
    }

    @Override
    public MovieDetails getDetails(String id) throws OmdbAccessException {
        logger.info(String.format("Retrieving details for  movie : [%s]", id));
        UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(omdbConfigurations.getApiDetails())
                .queryParam(ServiceConstants.TITLE_SEARCH, id)
                .queryParam(ServiceConstants.API_KEY, omdbConfigurations.getApiKey());
        try {
            ResponseEntity<MovieDetails> response =
                    restTemplate.getForEntity(uri.toUriString(), MovieDetails.class);

            logger.info(String.format("details for [%s] retrieved with status [%s]", id, response.getStatusCode()));
            return response.getBody();

        } catch (Exception ex) {
            logger.error("Error occur during retrieving details from OMDB API ." , ex);
            throw new OmdbAccessException(
                    String.format("Error receiving details for [%s] movie", id), ex);
        }
    }

    @Override
    public String getCover(String id) {
        return UriComponentsBuilder.fromHttpUrl(omdbConfigurations.getApiCover())
                .queryParam(ServiceConstants.TITLE_SEARCH, id)
                .queryParam(ServiceConstants.API_KEY, omdbConfigurations.getApiKey()).toUriString();
    }
}
