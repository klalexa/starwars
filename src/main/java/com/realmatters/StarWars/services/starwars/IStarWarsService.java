package com.realmatters.StarWars.services.starwars;

import com.realmatters.StarWars.constants.Order;
import com.realmatters.StarWars.model.Movie;

import java.util.List;

public interface IStarWarsService {

    List<Movie> getMovies(Order order);
    void save(String userId, List<String> moviesId);

}
