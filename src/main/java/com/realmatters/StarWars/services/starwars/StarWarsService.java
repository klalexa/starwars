package com.realmatters.StarWars.services.starwars;

import com.realmatters.StarWars.constants.Order;
import com.realmatters.StarWars.exceptions.OmdbAccessException;
import com.realmatters.StarWars.model.Movie;
import com.realmatters.StarWars.services.omdb.OmdbAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StarWarsService implements IStarWarsService {

    private static final Logger logger = LoggerFactory.getLogger(StarWarsService.class);

    @Autowired
    private OmdbAPI omdbAPI;

    @Override
    public List<Movie> getMovies(Order order) {
        try {
            Optional<List<Movie>> starWarsMovies = omdbAPI.getStarWarsMovies();
            if (!starWarsMovies.isPresent()) {
                return null;
            }
            List<Movie> movies = sort(starWarsMovies.get(), order);
            for (Movie movie : movies) {
                movie.setMovieDetails(omdbAPI.getDetails(movie.getImdbId()));
                movie.setCover(omdbAPI.getCover(movie.getImdbId()));
            }
            return movies;
        } catch (OmdbAccessException e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @Override
    public void save(String userId, List<String> moviesId) {

    }

    private List<Movie> sort(List<Movie> movies, Order order) {
        return movies.stream()
                .sorted(Comparator.comparing(movie ->
                        order.isMacheteOrder() ? movie.getPosition().getMachete() : movie.getPosition().getEpisode()))
                .collect(Collectors.toList());
    }
}
