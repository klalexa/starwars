package com.realmatters.StarWars.services.user;

import com.realmatters.StarWars.controllers.input.UserFavoriteMovie;
import com.realmatters.StarWars.entities.UsersFavorites;
import com.realmatters.StarWars.exceptions.DbException;
import com.realmatters.StarWars.repository.users.UsersRepository;
import com.realmatters.StarWars.services.starwars.StarWarsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsersPreferencesService implements IUsersPreferencesService {

    private static final Logger logger = LoggerFactory.getLogger(StarWarsService.class);

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public void save(UserFavoriteMovie userFavoriteMovie) throws DbException {
        logger.info("Saving user favorite movie");
        try {
            UsersFavorites usersFavorites = new UsersFavorites();
            usersFavorites.setUserid(userFavoriteMovie.getUserid());
            usersFavorites.setFavoriteMovie(userFavoriteMovie.getMovieid());
            usersRepository.save(usersFavorites);

        }catch (Exception ex) {
            logger.error(
                    String.format("Error saving for user [%s] his favorite movie [%s]",
                            userFavoriteMovie.getUserid(),
                            userFavoriteMovie.getMovieid()));

            throw new DbException("Error saving users favorite movie", ex);
        }
    }
}
