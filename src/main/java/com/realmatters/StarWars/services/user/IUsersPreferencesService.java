package com.realmatters.StarWars.services.user;

import com.realmatters.StarWars.controllers.input.UserFavoriteMovie;
import com.realmatters.StarWars.exceptions.DbException;

public interface IUsersPreferencesService {

    void save(UserFavoriteMovie userFavoriteMovie) throws DbException;
}
