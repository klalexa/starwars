package com.realmatters.StarWars.services.requestshistory;

import com.realmatters.StarWars.exceptions.DbException;

import javax.servlet.http.HttpServletRequest;

public interface IRequestHistoryService {

    void save(HttpServletRequest request) throws DbException;
}
