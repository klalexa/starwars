package com.realmatters.StarWars.services.requestshistory;

import com.realmatters.StarWars.entities.RequestHistory;
import com.realmatters.StarWars.model.UserRequest;
import com.realmatters.StarWars.exceptions.DbException;
import com.realmatters.StarWars.repository.reqeusthistory.RequestHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class RequestHistoryService implements IRequestHistoryService {

    private static final Logger logger = LoggerFactory.getLogger(RequestHistoryService.class);


    @Autowired
    private RequestHistoryRepository historyRepository;


    @Override
    public void save(HttpServletRequest request) throws DbException {
        String ip = request.getRemoteAddr();
        String queryString = request.getQueryString();
        logger.info(String.format("Saving request with ip [%s] with params [%s]", ip, queryString));
        try {
            RequestHistory requestHistory = new RequestHistory();
            UserRequest userRequest = new UserRequest();
            userRequest.setIp(ip);
            userRequest.setParams(queryString);
            userRequest.setApi(request.getRequestURI());
            requestHistory.setUserRequest(userRequest);
            historyRepository.save(requestHistory);
        } catch (Exception ex) {
            logger.error("Error saving request ", ex);
            throw new DbException("Error saving request with params [%s]", ex);
        }
    }
}
