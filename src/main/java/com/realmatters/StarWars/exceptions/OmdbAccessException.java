package com.realmatters.StarWars.exceptions;

public class OmdbAccessException extends Exception {

    public OmdbAccessException(String message, Throwable cause) {
        super(message, cause);
    }
}
