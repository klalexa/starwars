package com.realmatters.StarWars.services;

import com.realmatters.StarWars.config.OmdbTestConfig;
import com.realmatters.StarWars.exceptions.OmdbAccessException;
import com.realmatters.StarWars.services.omdb.OmdbService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        OmdbTestConfig.class
})
public class OmdbServiceTest {

    @Autowired
    @InjectMocks
    private OmdbService omdbService;

    @Mock
    private RestTemplate restTemplate;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = OmdbAccessException.class)
    public void getStarWarsMoviesWithExceptionTest() throws OmdbAccessException {
        // prepare
        when(restTemplate.exchange(anyString(),
                any(HttpMethod.class),
                any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenThrow(RestClientException.class);


        // test
        omdbService.getStarWarsMovies();
    }
}
