package com.realmatters.StarWars.config;

import com.realmatters.StarWars.services.omdb.OmdbService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

@Configuration
public class OmdbTestConfig {

    @Bean
    @Primary
    public OmdbService createOmdbService() {
        return new OmdbService();
    }

    @Bean
    @Primary
    public RestTemplate createRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    @Primary
    public OmdbConfigurations createOmdbConfigurations() {
        return new OmdbConfigurations();
    }
}
