# Star Wars API

## How to run the service

    The project ready to work with H2 ( in memory ) and MySql databases.
    
    If you work with H2 , you need modify application.properties file by comment spring.datasource 
        for mysql and uncomment spring.datasource for H2.
        
    If you work with MySql , verify that you have connection to a running instance of MySql server.
    
    1. Clone the project by running :   git clone https://bitbucket.org/klalexa/starwars.git
    2. Run: cd starwars
    3. Run:  sudo mvn clean package - to build the project and create a jar file. 
    5. Run: java -Dspring.datasource.server.ip=<mysql server ip> -Dspring.datasource.username=<user name> -Dspring.datasource.password=<passowrd> -jar target/StarWars-0.0.1-SNAPSHOT.jar
    for example : java -Dspring.datasource.server.ip=127.0.0.1 -Dspring.datasource.username=root -Dspring.datasource.password=root -jar target/StarWars-0.0.1-SNAPSHOT.jar
    6. In browser run :  http://localhost:8080
    